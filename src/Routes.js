import React from "react";
import { Switch, Route } from "react-router-dom";
import Screen1 from "./Pages/Screen1";
import Screen2 from "./Pages/Screen2";
import Screen3 from "./Pages/Screen3";

function Routes() {
  return (
    <div className="routes">

        <Switch>

      <Route path="/" exact={true}>
        <Screen1 />
      </Route>
      <Route path="/screen2">
        <Screen2 />
      </Route>
      <Route path="/screen3">
        <Screen3 />
      </Route>
        </Switch>
    </div>
  );
}

export default Routes;
