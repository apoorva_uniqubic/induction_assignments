import "./App.css";
import Routes from "./Routes";
import Header from './Header'
import Footer from './Footer'
import { Navbar, Container } from "react-bootstrap";
import { BrowserRouter as Router, Link, Route,Switch } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Router >
    <Header />
    <Routes />
    <Footer />
      </Router>
    </div>
  );
}

export default App;
