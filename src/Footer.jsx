import React, { Component } from "react";
import {Navbar,Container} from "react-bootstrap";
export default class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <Container>
          <Navbar expand="lg" variant="light" bg="light" fixed="bottom"> 
            <Navbar.Brand>Footer</Navbar.Brand>
          </Navbar>
        </Container>
      </div>
    );
  }
}
