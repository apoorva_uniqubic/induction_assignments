import React, { Component } from "react";
import {Navbar,Nav} from "react-bootstrap";
import {Link, BrowserRouter as Router} from 'react-router-dom'

let navStyle={
    color:"white",
    textDecoration:"none"

}

export default class Header extends Component {
  render() {
    return (
      <div className="header">
        <Navbar bg="dark" variant="dark" fixed="top">
          <Navbar.Brand >Navbar</Navbar.Brand>
          <Nav className="mr-auto">
            <Nav.Link ><Link style={navStyle} exact to="/" >Screen 1</Link></Nav.Link>
            <Nav.Link ><Link style={navStyle} to="/screen2">Screen 2</Link></Nav.Link>
            <Nav.Link ><Link style={navStyle} to="/screen3">Screen 3</Link></Nav.Link>
          </Nav>
        </Navbar>
      </div>
    );
  }
}
